import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';

if (!firebase.apps.length) {
  const config = {
    apiKey: "AIzaSyDGAEjF5FD9DRlFDXI27v-7ptlNArW_v4I",
    authDomain: "shutterminds-67dbf.firebaseapp.com",
    databaseURL: "https://shutterminds-67dbf.firebaseio.com",
    projectId: "shutterminds-67dbf",
    storageBucket: "shutterminds-67dbf.appspot.com",
    messagingSenderId: "929770958443",
    appId: "1:929770958443:web:11269cd93ad8b144264b2e",
    measurementId: "G-4NFGHE7R45"
  }


  firebase.initializeApp(config);

  const firestore = firebase.firestore();
}

const app = firebase.app();
const firestore = firebase.firestore();
const storage = firebase.storage();
const auth = app.auth();

export {firestore, app, storage,auth}
