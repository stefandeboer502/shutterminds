import Vue from 'vue';

import Login from '@/components/Login';
import Register from '@/components/Register';

import Header from '@/components/Header';


Vue.component(Login.name, Login);
Vue.component(Register.name, Register);

Vue.component(Header.name, Header);
