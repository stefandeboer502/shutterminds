const debug = require('debug')('Shutterminds:api');

import express from 'express';

import api from '@/api';

const app = express();

app.use((req, res, next) => {
  debug(`[${req.method}] ${req.url}`);

  next();
});

app.use(api.path, api.handler);

const port = Number(3000);

app.listen(port, () => debug(`Listing on http://localhost:3000`));
