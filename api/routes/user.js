import { Router, json } from 'express';

import { firestore } from '@/plugins/firebase';
import { api } from '@/api/util/express-helper';
import { User } from '@/api/routes/user';
import firebase from 'firebase';

const router = new Router();

router.get('/test', api(async req => {

  console.log('test');

}));
