import isPlainObject from 'lodash/isPlainObject';

/**
 * Wrap async function into middleware
 *
 * @param {Function} fn
 *
 * @returns {Function}
 */
export const api = fn => (req, res, next) => {
  fn(req, res, next)
    .then(result => {
      if (isPlainObject(result)) {
        res.status(result.status || 200);
      }

      return res.json(result);
    })
    .catch(exception => {
      // eslint-disable-next-line
      console.error(exception);

      res.status(exception.status || 500).json(isPlainObject(exception) ? exception : exception.toString());
    });
};

/**
 * Wrap async function into middleware
 *
 * @param {Function} fn
 *
 * @returns {Function}
 */
export const catchExceptions = fn => (req, res, next) => {
  fn(req, res, next)
    .catch(exception => {
      // eslint-disable-next-line
      console.error(exception);

      res.status(exception.status || 500).json(isPlainObject(exception) ? exception : exception.toString());
    });
};
