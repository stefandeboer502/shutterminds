module.exports = {
  autoProcessQueue: false,
  url: process.env.BASE_URL || 'http://localhost/',
  maxFilesize: 3,
  maxFiles: 20,
  dictDefaultMessage: "Drop files here to upload",
  dictFallbackMessage: "Your browser does not support drag'n'drop file uploads.",
  dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
  dictFileTooBig: "File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.",
  dictInvalidFileType: "You can't upload files of this type.",
  dictResponseError: "Server responded with {{statusCode}} code.",
  dictCancelUpload: "Cancel upload",
  dictUploadCanceled: "Canceled upload",
  dictCancelUploadConfirmation: "Are you sure you want to cancel this upload?",
  dictRemoveFile: "Remove file",
  dictMaxFilesExceeded: "You can not upload any more files.",
  previewTemplate:
    '<div class="dropzone-preview">' +
    '<div class="item">' +
    '<img data-dz-thumbnail />' +
    '<div class="details">' +
    '<div class="filename"><span data-dz-name></span></div>' +
    '<div class="filesize" data-dz-size></div>' +
    '</div>' +
    '<div data-dz-remove class="remove" title="Remove file"><span>Remove</span><i class="mdi mdi-close-circle"></i></div>' +
    '</div>' +
    '<div class="error-message"><span data-dz-errormessage></span></div>' +
    '</div>'
}
