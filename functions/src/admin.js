const functions = require('firebase-functions');
const admin = require('firebase-admin');
const serviceAccount = require('../serviceAccountKey.json');

const app = admin.apps.length
  ? admin.app()
  : admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: functions.config().database.url || process.env.FIREBASE_CONFIG.database.url, // default firebase var
    storageBucket: functions.config().storage.bucket || process.env.FIREBASE_CONFIG.storage.bucket // default firebase var
  });

module.exports = {
  app,
  firestore: app.firestore(),
  db: app.database(),
  storage: app.storage(),
  auth: app.auth()
};

