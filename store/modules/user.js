import { auth, firestore } from '@/plugins/firebase'
import Cookies from 'js-cookie'

export const state = () => ({
  uid: null,
  user: null,
  claims: {
    admin: false,
    moderator: false
  }
});

export const getters = {
  uid(state) {
    if (state.user && state.user.uid) {
      return state.user.uid;
    }

    return null;
  },
  userName(state) {
    if (state.user && state.user.uid) {
      return state.user.name;
    }

    return null;
  },
  user(state) {
    return state.user;
  },
  isAuthenticated(state) {
    return !!state.user && !!state.user.uid;
  },
  isAdmin(state) {
    return !!state.claims.admin;
  },
  isModerator(state) {
    return !!state.claims.moderator || !!state.claims.admin;
  }
};

export const actions = {
  async login({ dispatch, commit }, user) {
    const token = await auth.currentUser.getIdToken(true);
    const idTokenResult = await auth.currentUser.getIdTokenResult();

    let approved = false;
    const userData = await firestore.collection('users').doc(user.email).get();
    if (userData) {
      approved = userData.data().isApproved
    }

    const userInfo = {
      name: user.displayname || null,
      email: user.email || null,
      avatar: user.photoURL || null,
      uid: user.uid,
      approved
    };
    Cookies.set('access_token', token); // saving token in cookie for server rendering
    await commit('saveUID', userInfo.uid);
    await commit('setUSER', userInfo);
    await commit('setCLAIMS', {admin: idTokenResult.claims.admin, moderator: idTokenResult.claims.moderator});

    location.reload();
  },
  async logout ({dispatch}) {
    await auth.signOut();

    Cookies.remove('access_token');
    dispatch('saveUID', null);
    dispatch('setUSER', null);
    location.reload();
  },
  saveUID({commit}, uid) {
    commit('saveUID', uid);
  },
    setUSER({commit}, user) {
    commit('setUSER', user);
  }
};

export const mutations = {
  saveUID (state, uid) {
    state.uid = uid;
  },
  setUSER (state, user) {
    state.user = user;
  },
  setCLAIMS (state, claims) {
    state.claims = claims;
  },
  setAPPROVED (state, approved) {
    state.approved = approved;
  }
};

