import { firestore } from '~/plugins/firebase'
import { getUserFromCookie } from '@/helpers';

export const state = () => ({
  status: null,
  error: null,
});

export const mutations = {
  setStatus(state, payload) {
    state.status = payload;
  },
  setError(state, payload) {
    state.error = payload;
  },
};

export const getters = {
  status(state) {
    return state.status;
  },
  error(state) {
    return state.error;
  },
};

export const actions = {
  async nuxtServerInit({ dispatch, commit }, { req }) {

    //console.log(req.headers.cookie);
    const user = getUserFromCookie(req);
    if (user) {
      let approved = false
      const userData = await firestore.collection('users').doc(user.email).get();

      //console.log(user.firebase);
      if (userData && userData.data()) {
        approved = userData.data().isApproved
      }

      await commit('modules/user/setUSER', {
        name: user.name || null,
        email: user.email || user.user_id,
        avatar: user.picture || null,
        uid: user.user_id,
        approved
      });
      await commit('modules/user/setCLAIMS', { admin: user.admin, moderator: user.moderator });
    }
  }
};
